# ddns

#### 介绍
将用户的动态IP地址映射到域名解析服务，目前支持阿里云DNS。

#### 软件架构
软件架构说明
通过阿里云SDK，定时向阿里云DNS更新用户动态IP


#### 安装教程

1. 修改配置文件

   将`conf/.env.example`修改为`conf/.env`

2. 编译
```shell
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ddns .
```
2.  编写DockerFile
```dockerfile
FROM scratch
COPY conf /conf
ADD ddns /
CMD ["/ddns"]
```
3.  创建容器
```shell
docker build -t iddns -f DockerFile.scratch .
docker run -itd --restart=always --name ddns iddns
```



#### 使用说明

1.  配置文件`conf/.env`中，`SUBDOMAINS`是等待动态解析的域名，多个域名用逗号`,`分隔。
2.  `TickerDescribe`是查询`SUBDOMAINS`域名解析记录的定时时长。默认每600秒查询一次上述域名的解析记录。
3.  `TickerUpdate`是获取当前主机公网IP的定时轮询时长。如果当前主机公网IP发生改变，那么主动推送域名解析更新。默认每60秒查询一次当前主机公网IP。
4.  `GLOBAL_IP_SERVER`是解析公网IP的服务器，默认是`http://ip.dhcp.cn/?ip`


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
