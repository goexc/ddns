package ddns

import (
	"fmt"
	"gitee.com/goexc/ddns/variables"
	"testing"
)

func TestClient_Run(t *testing.T) {
	config := Config{
		QueryInterval:   10,
		PollingInterval: 1,
		RegionId:        "cn-beijing",
		AccessKeyId:     "",
		AccessKeySecret: "",
		Domains:         "",
	}
	client, e := NewClient(config)
	if e != nil {
		fmt.Println("客户端初始化失败：", e.Error())
		return
	}

	go func() {
		for msg := range variables.Msg {
			fmt.Println(msg)
		}
	}()

	if e = client.Run(); e != nil {
		fmt.Println("服务器错误：", e.Error())
	}
}
