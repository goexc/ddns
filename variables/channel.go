package variables

var Msg chan string

func init() {
	Msg = make(chan string, 1000)
}
