package utils

import (
	"strings"
)

type AliErrResponse struct {
	Status int    `json:"status"`
	Msg    string `json:"msg"`
	Title     string `json:"Title"`
	ErrorCode string `json:"ErrorCode"`
	Recommend string `json:"Recommend"`
	RequestId string `json:"RequestId"`
	Message   string `json:"Message"`
}

func ParsingAliErr(err error) (alierr AliErrResponse, ok bool) {
	msgs := strings.Split(err.Error(), "\n")
	for idx, msg := range msgs {
		if strings.Contains(msg, ": "){
			msgs[idx] = strings.Split(msg, ": ")[1]
		}
	}

	if len(msgs) < 5 {
		return
	}

	alierr.Title = msgs[0]
	alierr.ErrorCode = msgs[1]
	alierr.Recommend = msgs[2]
	alierr.RequestId = msgs[3]
	alierr.Message = msgs[4]

	return alierr,true
}
