package variables

import "github.com/aliyun/alibaba-cloud-sdk-go/services/alidns"

var Domains map[string]*alidns.Record

func init()  {
	Domains = make(map[string]*alidns.Record)
}
